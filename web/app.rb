require 'bundler/setup'
require 'pstore'
require 'sinatra'
require 'sinatra/reloader'
require 'haml'
require 'active_support/time'
require 'pp'

require "./config.rb"

#config = Config.instance

get '/civil_war' do
	@title = "バドン内乱用"
	@last_check_date_time, @last_publish_date_time = get_date_times
	@next_check_date_time = @last_check_date_time.since(CHECKER_INTERVAL.minutes)
	@next_publish_date_time = @last_publish_date_time.since(PUBLISHER_INTERVAL.minutes)
	@tournaments = get_tournamets
	haml :index
end

post '/civil_war' do

	tournaments = get_tournamets

	tournament = {
		title: params[:title],
		datetime: Time.parse(params[:date] + ' ' + params[:time]),
		entries: [],
	}

	tournaments << tournament

	save_tournamets(tournaments)

	redirect '/civil_war'
end

post '/civil_war/delete/:seq' do |seq|

	tournaments = get_tournamets

	tournaments.delete_at(seq.to_i)

	save_tournamets(tournaments)

	redirect '/civil_war'
end

def get_tournamets

	db = PStore.new(PSTORE_PATH)

	tournaments = nil
	db.transaction{|pstore|
		tournaments = pstore["tournaments"] || []
	}
	
	return tournaments
end

def save_tournamets(tournaments)

	db = PStore.new(PSTORE_PATH)
	db.transaction{|pstore|
		pstore["tournaments"] = tournaments
	}

end

def get_date_times

	db = PStore.new(PSTORE_PATH)

	last_check_date_time = nil
	last_publish_date_time = nil

	db.transaction{|pstore|
		last_check_date_time = pstore["last_check_date_time"] || Time.now
		last_publish_date_time = pstore["last_publish_date_time"] || Time.now
	}
	
	return last_check_date_time, last_publish_date_time
end

__END__

@@ layout
!!! 5
%html
	%head
		%title= @title
	%body
		%h1= @title
		%div= yield

@@ index
%form{ method: "post", action: "/civil_war" }
	%span
		大会名：
	%input{ type: "text", name: "title" }
	%br
	%span
		開催日時：
	%input{ type: "date", name: "date" }
	%input{ type: "time", name: "time" }
	%br
	%button{ type: "submit" } 登録
%br
%br
%div
	%h2 登録済みの大会
	%div
		次回エントリーチェック時間：#{@next_check_date_time.strftime("%Y/%m/%d %H:%M")}（前回：#{@last_check_date_time.strftime("%Y/%m/%d %H:%M")}）
		%br
		次回開催チェック時間：#{@next_publish_date_time.strftime("%Y/%m/%d %H:%M")}（前回：#{@last_publish_date_time.strftime("%Y/%m/%d %H:%M")}）
	%br
	- seq = @tournaments.size - 1
	- for tournament in @tournaments.reverse
		%div
			= tournament[:datetime].strftime("%Y/%m/%d %H:%M")
			%b= tournament[:title]
			%br
			エントリー締め切り：#{tournament[:datetime].ago(10.minutes).strftime("%Y/%m/%d %H:%M")}
			%br
			エントリー数：#{tournament[:entries].count}
			%form{ method: "post", action: "/civil_war/delete/#{seq}"}
				%button{ type: "submit", onclick: "return confirm('削除しますか？')" } 削除
			- for entry in tournament.entries
				%div
					/TODO: 画像のせたい
			%br
			- seq -= 1
