require 'bundler/setup'
require 'mastodon'
require 'pstore'
require 'open-uri'
require 'active_support/time'
require 'pp'

require "./config.rb"

#setup
if !Dir.exists?("images") then
	Dir.mkdir("images");
end

#load status
config = Config.instance
db = PStore.new(PSTORE_PATH)

latest_max_id = 0
tournaments = []
db.transaction{|pstore|
	latest_max_id = pstore["latest_max_id"] || 0
	tournaments = pstore["tournaments"] || []
}

tournament = nil
tournaments.each { |t|
	if t[:datetime] > Time.now then
		tournament = t
		break
	end
}

if tournament.nil? then
	exit
end

#process main
#新着通知からDMを取得
mstdn = Mastodon::REST::Client.new(base_url: MASTODON_HOST, bearer_token: config.mastodon_token)
notifications = mstdn.notifications({
	exclude_types: [:follow, :favourite, :reblog],
	min_id: latest_max_id
})

max_id = latest_max_id;
now = Time.now
notifications.each { |notification|
	status = notification.status;
	if status.visibility == "direct" then
		print notification.status.content + "\n"

		#DMからエントリー情報を作成
		entry_name = status.account.display_name
		if entry_name.nil? || entry_name == "" then
			entry_name = status.account.username
		end
		entry = {
			id: status.account.id,
			name: entry_name,
			comment: status.content,
			images: [],
		}

		status.media_attachments.each { |media|
			if media.type == 'image' then
				#print "#{media.url}\n"

				url = media.url
				filename = media.id;
				 
				open(url, { read_timeout: nil }) do |file|
				  open("images/" + filename, "w+b") do |out|
				    out.write(file.read)
				  end
				end

				entry[:images] << filename
			end
		}

		#既に同じユーザーからのエントリーがあれば上書き、なければ追加
		entry_index = tournament[:entries].find_index{ |e|
			e[:id] == entry[:id]
		}
		if entry_index then
			tournament[:entries][entry_index] = entry;
		else
			tournament[:entries] << entry
		end
	end

	id = notification.id.to_i
	if max_id < id then
		max_id = id
	end
}

#save status
db.transaction{|pstore|
	pstore["last_check_date_time"] = now
	pstore["latest_max_id"] = max_id
	pstore["tournaments"] = tournaments
}
