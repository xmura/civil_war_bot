require 'bundler/setup'
require 'mastodon'
require 'pstore'
require 'active_support/time'
require 'pp'

require "./config.rb"

#load status
config = Config.instance
db = PStore.new(PSTORE_PATH)

latest_publish_id = 0
tournaments = []
db.transaction{|pstore|
	tournaments = pstore["tournaments"] || []
}

#process main
now = Time.now
tournaments.each { |tournament|

	pp tournament[:title], tournament[:datetime]
	
	if tournament[:datetime] <= now then
		next
	end

	if tournament[:datetime] > (now.since(10.minutes) ) then
		next
	end

	#pp tournament

	mstdn = Mastodon::REST::Client.new(
		base_url: MASTODON_HOST,
		bearer_token: config.mastodon_token,
		timeout: {
      		connect: 60,
      		read: 300,
      		write: 300,
    	}
	)
	tournament[:entries].each { |entry|

		comment = /(?<=>)[^>]*(?=<\/p>)/.match(entry[:comment]).to_a[0];
		#pp comment
		#next

		mediaIds = [];
		entry[:images].each{ |image|
			uploadMedia = mstdn.upload_media('images/'+image)
			mediaIds << uploadMedia.id
		}

		mstdn.create_status("#{comment}\nBy #{entry[:name]}", {
			media_ids: mediaIds
			#visibility: "private"
		})
	}

	break
}

#save status
db.transaction{|pstore|
	pstore["last_publish_date_time"] = now
	#pstore["tournaments"] = tournaments
}
