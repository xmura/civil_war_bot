require 'bundler/setup'
require 'mastodon'
require 'oauth2'
require 'pstore'
require 'active_support/time'

require "./config.rb"

print "user_id: "
mastodon_user_id = gets.chomp
print "password: "
mastodon_password = gets.chomp

mastodon = Mastodon::REST::Client.new(base_url: MASTODON_HOST)
app = mastodon.create_app(MASTODON_APP_NAME, 'urn:ietf:wg:oauth:2.0:oob', 'read write follow')

client = OAuth2::Client.new(app.client_id, app.client_secret, site: MASTODON_HOST)
token = client.password.get_token(mastodon_user_id, mastodon_password, scope: 'read write follow')

print "client_id: #{app.client_id}\n"
print "client_secret: #{app.client_secret}\n"
print "access_token: #{token.token}\n"

if token.nil? || token.token.nil?
	puts "get_token failed"
	exit
end

db = PStore.new(PSTORE_PATH)

db.transaction{|pstore|
	pstore["mastodon_token"] = token.token
}
