require 'singleton'
require 'pstore'

require "./const.rb"

class Config
	include Singleton

	attr_accessor :mastodon_token

	def config
		Config.instance
	end

	def initialize
		@mastodon_token = nil

		db = PStore.new(PSTORE_PATH)
		db.transaction{|pstore|
			@mastodon_token = pstore["mastodon_token"] || ENV['MASTODON_TOKEN']
		}
	end

end